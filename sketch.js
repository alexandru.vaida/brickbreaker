
 
//Balls x and Y Coordinates
var xBall = 230;
var yBall = 200;
//Paddle X Coordinate
var xPaddle = 100;
var yPaddle = 500 - 3;
//Ball Speed
var xSpeed = 5;
var ySpeed =5;


//Brick Variables
var brickWidth = 40;
var brickHeight = 20;
var numRows = 5;
var numColumns = 13;
var totalBricks = numRows * numColumns;
var brickList = [];



function setup() {
createCanvas(500, 500);
noCursor();
  
rectMode(CENTER);
  
}

function draw() {
  background(0,0,0);
 
  fill(0, 0, 255);
  ellipse(xBall, yBall, 15)
  
  fill(255, 150, 0);
  rect(xPaddle, yPaddle, 80, 5);
  
   for(var i = 0; i < brickHeight * numRows; i += brickHeight)
    {
      for(var j = 0; j < brickWidth * numColumns; j += brickWidth)
        {
        brickList.push(new brick(j + 20, i + 10));
        }
     }
  
  for(var k = 0; k < totalBricks; k++){
    brickList[k].display();
    brickList[k].collision();
  }
  
  //Move the Paddle
  movePaddle();
  //Move the Ball
  moveBall();
}

function movePaddle() {
  
  if (mouseX >= 40 && mouseX <= width - 40) {//Mouse is within canvas
  xPaddle = mouseX;
  } else if(mouseX < 40) {//If mouse is too far left
    xPaddle = 40;
  } else if(mouseX > width - 40) {//If mouse is too far right
    xPaddle = width - 40;
  }
  
}

function moveBall() {
  xBall += xSpeed;
  yBall += ySpeed;
  
  //Ball Bouncing X Direction
  if(xBall <= 10 || xBall >= width - 10) {
    if(xSpeed < 10 && xSpeed > -10) {
      xSpeed = xSpeed * (-1);
    } else {
      xSpeed = xSpeed * (-1);
    }
  }
  
    //Ball Bouncing Y Direction
  if(yBall <= 10) {
    if(ySpeed < 10 && ySpeed > -10) {
      ySpeed = ySpeed * (-1);
    } else {
      ySpeed = ySpeed * (-1);
    }
  }
  
   //Hit Detection
  if(yBall >= height - 10) {
    if(xBall <= xPaddle + 50 && xBall >= xPaddle - 50) {//If the ball hits the paddle
      if(ySpeed < 10 && ySpeed > - 10) {//Increasing the speed
        ySpeed = ySpeed * (-1);
      } else {
        ySpeed = ySpeed * (-1);
      }
    } else {//If the ball missses the paddle
      textAlign(CENTER);
      textFont("Open Sans");
      textSize(50);
      fill(255);
      textStyle(BOLD);
      text("Game Over", width / 2, height / 2);
      noLoop();//STOP THE GAME LOOP
    }
  }
  
}

//brick CLASS
function brick(xBrick, yBrick) {
  this.x = xBrick;
  this.y = yBrick;
  this.myWidth = brickWidth;
  this.myHeight = brickHeight;
  this.myColor = color("blue");
  
  this.display = function() {
    fill(this.myColor);
    rect(this.x, this.y, brickWidth, brickHeight);
  }
  
  this.collision = function() {
    
    if(yBall < this.y + brickHeight) {
      if(xBall < this.x + (brickWidth / 2) && xBall > this.x - (brickWidth / 2))
        {
      this.x = -20;
      this.y = -20;
      ySpeed = ySpeed * -1;
    }
    }
  }
  
}